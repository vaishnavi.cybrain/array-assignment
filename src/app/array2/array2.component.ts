import { Component, OnInit } from '@angular/core';
 
export class Stack<T> {
  private items: T[];

  constructor() {
    this.items = [];
  }

  push(item: T) {
    this.items.push(item);
  }

  pop(): T | undefined {
    return this.items.pop();
  }

  isEmpty(): boolean {
    return this.items.length === 0;
  }

  peek(): T | undefined {
    return this.items[this.items.length - 1];
  }
}

interface Person {
  Id: number;
  FirstName: string;
  LastName: string;
  Email: string;
  ContactNo: string;}

@Component({
  selector: 'app-array2',
  templateUrl: './array2.component.html',
  styleUrls: ['./array2.component.scss']
})
export class Array2Component  implements OnInit {
// Define a 2D array with object keys
DataSet: { Id: number, FirstName: string, LastName: string, Email: string, ContactNo: string }[] = [
  { Id: 1, FirstName: 'John', LastName: 'Doe', Email: 'john.doe@example.com', ContactNo: '555-1234' },
  { Id: 2, FirstName: 'hum', LastName: 'jol', Email: 'jole.doe@example.com', ContactNo: '555-5678' },
  { Id: 3, FirstName: 'Bob', LastName: 'Smith', Email: 'bob.smith@example.com', ContactNo: '555-9876' },
];
DataSet1: { Id: number, FirstName: string, LastName: string, Email: string, ContactNo: string }[] = [
  { Id: 1, FirstName: 'John', LastName: 'Doe', Email: 'john.doe@example.com', ContactNo: '555-1234' },
  { Id: 2, FirstName: 'iron', LastName: 'ban', Email: 'iron.ban@example.com', ContactNo: '555-5678' },
  { Id: 3, FirstName: 'kum', LastName: 'kem', Email: 'kum.kem@example.com', ContactNo: '555-9876' },
];

// push 
pp=this.DataSet.push(
  {
    Id: 4,
    FirstName: "Alice",
    LastName: "Johnson",
    Email: "alicejohnson@example.com",
    ContactNo: "555-555-5555"
  },
  {
    Id: 5,
    FirstName: "Bob",
    LastName: "Brown",
    Email: "bobbrown@example.com",
    ContactNo: "555-555-5555"
  }
);


newElement = { Id: 7, FirstName: 'Bob', LastName: 'Smith', Email: 'bob.smith@example.com', ContactNo: '555-9876' };

arr1 = [1, 2, 3];
arr2=[4, 5, 6];

removedElement = this.DataSet.pop();

myStack: Stack<number> = new Stack<number>();
 
originalArray = [123, 2234, 345, 445, 578];
 copyArray = this.originalArray.slice();

myArray =[];
  myArray3 = ['apple', 'banana', 'cherry'];
indextoreplace=2;
newItem=6;

guu=this.DataSet.map((item) => item.Id);
constructor(){
  
  
  this.myStack.push(1);
  this.myStack.push(2);
  this.myStack.push(3);

  console.log(this.myStack.pop()); 
  console.log(this.myStack.pop()); 
  console.log(this.myStack.peek());
  console.log(this.myStack.isEmpty());
  
 
  }
  


ngOnInit(){
  //4
  const length=this.DataSet.length
  console.log(length)
  //5
  //No, the push() function cannot be used to add elements to the beginning of an array
  //6
  if(!this.DataSet.some(element => element.Id ===this.newElement.Id)){
    this.DataSet.push(this.newElement);
  }
  //7
this.arr1.push(...this.arr2);

//8
  console.log(this.DataSet);
  console.log(this.removedElement); 


  //9
 // The pop() function modifies the original array and returns the removed element.

 //10
 const myArray: any[] = [];

if (this.myArray.pop() === undefined) {
  console.log("Array is empty");
} else {
  console.log("Array is not empty");
}
//11 No,
//13
// Define an array
let arr = ["apple", "banana", "cherry", "date", "elderberry"];

// Remove elements at index 2 and 3
arr.splice(2, 3);

console.log(arr); // ["apple", "banana", "elderberry"]
//14
let array = [1, 2, 3, 4, 5, 6];
array.splice(2, 3); // Remove elements from index 2 to 3 (2 elements)
console.log(array); // Output: [1, 2,5, 6]
//15
//The splice() function in Angular returns an array of the deleted elements, or an empty array if no elements are deleted.
//16

console.log(this.myArray3); // Output: ['apple', 'orange', 'peach', 'cherry']

//17  slice() returns a new array containing a portion of the original array. It takes two optional arguments: start index and end index (exclusive). If no arguments are provided, it will return a copy of the original array. The original array is not modified by this method.
//17slice() returns a new array containing a portion of the original array. It takes two optional arguments: start index and end index (exclusive). If no arguments are provided, it will return a copy of the original array. The original array is not modified by this method.





//18
console.log(this.copyArray)
//19
this.DataSet.sort((a, b) => b.Id - a.Id);
console.log(this.DataSet)

//20
console.log(this.guu)

}
} 









  



